﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;

namespace Webborad
{
    public class clsData
    {
        public static SqlDataReader DtReader(String strSQL)
        {
            SqlCommand objCmd = new SqlCommand();
            SqlDataReader dtReader;
            clsDB.sqlserverrxcon(ref clsDB.objConn);

            objCmd = new SqlCommand(strSQL, clsDB.objConn);
            dtReader = objCmd.ExecuteReader();
            return dtReader; //*** Return DataReader ***//
        }

        public static bool ExecuteDTpara(string sql, SqlParameterCollection parameters)
        {
            SqlCommand objCmd;
            bool ok;
            clsDB.sqlserverrxcon(ref clsDB.objConn);
            try
            {
                objCmd = new SqlCommand(sql, clsDB.objConn);
                foreach (SqlParameter param in parameters)
                {
                    objCmd.Parameters.Add(param.ParameterName, param.SqlDbType).Value = param.Value;
                }
                objCmd.ExecuteNonQuery();
                ok = true;
            }
            catch
            {
                ok = false;
            }
            clsDB.closesqlcon(ref clsDB.objConn);
            return ok;
        }

        public static DataSet DtSet(String strSQL)
        {
            SqlCommand objCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();
            clsDB.sqlserverrxcon(ref clsDB.objConn);

            objCmd = new SqlCommand();
            objCmd.Connection = clsDB.objConn;
            objCmd.CommandText = strSQL;
            objCmd.CommandType = CommandType.Text;

            dtAdapter.SelectCommand = objCmd;
            dtAdapter.Fill(ds);
            return ds;   //*** Return DataSet ***//
        }

        public static DataTable DtTable(String strSQL)
        {
            SqlDataAdapter dtAdapter;
            DataTable dt = new DataTable();
            clsDB.sqlserverrxcon(ref clsDB.objConn);

            dtAdapter = new SqlDataAdapter(strSQL, clsDB.objConn);
            dtAdapter.Fill(dt);
            return dt; //*** Return DataTable ***//
        }

        public static bool Login(string user, string pass)
        {
            SqlCommand cmd;
            bool ok = false;
            clsDB.sqlserverrxcon(ref clsDB.objConn);
            try
            {
                cmd = new SqlCommand("SELECT COUNT(*) FROM wb_user WHERE username = @user AND password = @pass", clsDB.objConn);
                cmd.Parameters.Add("@user", SqlDbType.NVarChar).Value = user;
                cmd.Parameters.Add("@pass", SqlDbType.NVarChar).Value = pass;
                if (clsDB.DBNullInt(cmd.ExecuteScalar()) > 0)
                {
                    ok = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            clsDB.closesqlcon(ref clsDB.objConn);
            return ok;
        }

        public static string getCategoryName(int category_id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT category_name FROM wb_category " +
            "WHERE category_delete = 'False' AND category_id = " + category_id + " ";
            dt = clsData.DtTable(strSQL);
            string name = dt.Rows[0]["category_name"].ToString();
            return name;
        }

        public static DataTable getCategoryAll()
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_category " +
            "WHERE category_delete = 'False' " +
            "ORDER BY category_id ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getCategoryByForumID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT category_id FROM wb_forum " +
            "WHERE forum_delete = 'False' AND forum_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static string getForumName(int forum_id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT forum_name FROM wb_forum " +
            "WHERE forum_delete = 'False' AND forum_id = " + forum_id + " ";
            dt = clsData.DtTable(strSQL);
            string name = dt.Rows[0]["forum_name"].ToString();
            return name;
        }

        public static DataTable getForumInCategory(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_forum " +
            "WHERE forum_delete = 'False' AND category_id = " + id + " " +
            "ORDER BY forum_id DESC";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getForumAll()
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_forum " +
            "WHERE forum_delete = 'False' " +
            "ORDER BY forum_id DESC";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getForumByID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_forum " +
            "WHERE forum_delete = 'False' AND forum_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getTopicAll()
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_topic " +
            "WHERE topic_delete = 'False' " +
            "ORDER BY topic_id DESC";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getTopicInForum(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_topic " +
            "WHERE topic_delete = 'False' AND forum_id = " + id + " " +
            "ORDER BY topic_id DESC";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getTopicBySearch(string search_txt)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_topic " +
            "WHERE topic_delete = 'False' AND topic_name LIKE '%" + search_txt + "%' " +
            "ORDER BY topic_id DESC";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getTopicByID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_topic " +
            "WHERE topic_delete = 'False' AND topic_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getTopicByUserID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_topic " +
            "WHERE topic_delete = 'False' AND user_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getCommentAll()
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_comment " +
            "WHERE comment_delete = 'False' " +
            "ORDER BY comment_id ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getCommentInTopic(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_comment " +
            "WHERE comment_delete = 'False' AND topic_id = " + id + " " +
            "ORDER BY comment_id ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getCommentByID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_comment " +
            "WHERE comment_delete = 'False' AND comment_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getUserAll()
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_user " +
            "WHERE user_delete = 'False' " +
            "ORDER BY user_id ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable getUserByID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT * FROM wb_user " +
            "WHERE user_delete = 'False' AND user_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static string getUserShowName(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT show_name FROM wb_user " +
            "WHERE user_delete = 'False' AND user_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            string name = dt.Rows[0]["show_name"].ToString();
            return name;
        }

        public static string getUserImg(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT user_pic FROM wb_user " +
            "WHERE user_delete = 'False' AND user_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            string pic = dt.Rows[0]["user_pic"].ToString();
            return pic;
        }

        public static DataTable getUserRoleAll()
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT wb_user.* , wb_role.*, wb_user_role.* FROM wb_user " +
            "INNER JOIN wb_user_role ON wb_user_role.user_id = wb_user.user_id " +
            "INNER JOIN wb_role ON wb_role.role_id = wb_user_role.role_id " +
            "WHERE wb_user.user_delete = 'False' AND wb_user_role.user_role_delete = 'False' " +
            "ORDER BY wb_user.user_id ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        //public static DataTable getUserRoleByID(int id)
        //{
        //    DataTable dt = new DataTable();
        //    String strSQL = string.Empty;
        //    strSQL = "SELECT wb_user.* , wb_role.*, wb_user_role.* FROM wb_user " +
        //    "INNER JOIN wb_user_role ON wb_user_role.user_id = wb_user.user_id " +
        //    "INNER JOIN wb_role ON wb_role.role_id = wb_user_role.role_id " +
        //    "WHERE wb_user.user_delete = 'False' AND wb_user.user_id = " + id + " ";
        //    dt = clsData.DtTable(strSQL);
        //    return dt;
        //}

        public static string getRoleByUserID(int id)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = " SELECT wb_role.role_name FROM wb_user " +
                    " INNER JOIN wb_user_role ON wb_user_role.user_id = wb_user.user_id " +
                    " INNER JOIN wb_role ON wb_role.role_id = wb_user_role.role_id " +
                    " WHERE wb_user.user_delete = 'False' AND wb_user.user_id = " + id + " ";
            dt = clsData.DtTable(strSQL);
            string role = dt.Rows[0]["role_name"].ToString();
            return role;
        }

        public static void SaveLog(string table, string action, int user_id, int referent_id)
        {
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                //** Insert log**//
                strSQL = "INSERT INTO wb_log (table_name, action, date, user_id, referent_id) " + " VALUES " + " (@table, @action, @date, @user, @referent)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@table", SqlDbType.NVarChar).Value = "wb_" + table;
                objCmd.Parameters.Add("@action", SqlDbType.NVarChar).Value = action;
                objCmd.Parameters.Add("@date", SqlDbType.DateTime).Value = DateTime.Now;
                objCmd.Parameters.Add("@user", SqlDbType.Int).Value = user_id;
                objCmd.Parameters.Add("@referent", SqlDbType.Int).Value = referent_id;
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool UpdateCategory(int id, string name)
        {
            bool ok = false;
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                strSQL = "UPDATE wb_category SET category_name = @name " +
                              " WHERE category_id = " + id + "";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar).Value = name;
                objCmd.ExecuteNonQuery();
                ok = true;

                //** Insert log**//
                clsData.SaveLog("category", "EDIT CATEGORY", 1, id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ok;
        }

        public static bool UpdateUser(int id, string user, string pass, string show, string first, string sur, string gender, string mail, string tel, string address)
        {
            bool ok = false;
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                strSQL = "UPDATE wb_user SET username = @user " +
                              " ,password = @pass " +
                              " ,show_name = @show " +
                              " ,firstname = @first " +
                              " ,surname = @sur " +
                              " ,gender_id = @gender " +
                              " ,email = @mail " +
                              " ,tel = @tel " +
                              " ,address = @address " +
                              " WHERE user_id = " + id + "";

                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@user", System.Data.SqlDbType.NVarChar).Value = user;
                objCmd.Parameters.Add("@pass", System.Data.SqlDbType.NVarChar).Value = pass;
                objCmd.Parameters.Add("@show", System.Data.SqlDbType.NVarChar).Value = show;
                objCmd.Parameters.Add("@first", System.Data.SqlDbType.NVarChar).Value = first;
                objCmd.Parameters.Add("@sur", System.Data.SqlDbType.NVarChar).Value = sur;
                objCmd.Parameters.Add("@gender", System.Data.SqlDbType.Int).Value = Convert.ToInt32(gender);
                objCmd.Parameters.Add("@mail", System.Data.SqlDbType.NVarChar).Value = mail;
                objCmd.Parameters.Add("@tel", System.Data.SqlDbType.NVarChar).Value = tel;
                objCmd.Parameters.Add("@address", System.Data.SqlDbType.NVarChar).Value = address;
                objCmd.ExecuteNonQuery();
                ok = true;

                //** Insert log**//
                clsData.SaveLog("user", "EDIT", 1, id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ok;
        }

        public static bool UpdateUserRole(int id, int role)
        {
            bool ok = false;
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                strSQL = "UPDATE wb_user_role SET role_id = @role " +
                              " WHERE user_id = " + id + "";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@role", System.Data.SqlDbType.Int).Value = role;
                objCmd.ExecuteNonQuery();
                ok = true;

                //** Insert log**//
                clsData.SaveLog("user_role", "EDIT ROLE", 1, id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ok;
        }

        public static bool DeleteDataByID(string table, int id)
        {
            bool ok = false;
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                strSQL = "UPDATE wb_" + table + " SET " + table + "_delete = @delete WHERE " + table + "_id = @id";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                objCmd.Parameters.Add("@delete", System.Data.SqlDbType.Bit).Value = true;
                objCmd.ExecuteNonQuery();
                ok = true;

                //** Insert log**//
                clsData.SaveLog(table, "DELETE", 1, id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ok;
        }

        public static Bitmap resizeImage(Image image)
        {
            int new_height = 500;
            int new_width = 510;
            System.Drawing.Bitmap objBitmap = default(System.Drawing.Bitmap);
            if (new_height > 0)
            {
                objBitmap = new System.Drawing.Bitmap(image, new_width, new_height);
            }
            else
            {
                if (image.Width > new_width)
                {
                    double ratio = image.Height / image.Width;
                    new_height = (int)ratio * (int)new_width;
                    objBitmap = new System.Drawing.Bitmap(image, new_width, new_height);
                }
                else
                {
                    objBitmap = new System.Drawing.Bitmap(image);
                }
            }
            return objBitmap;
        }

        public static DataTable Forget_Pass(string email)
        {
            DataTable dt = new DataTable();
            String strSQL = string.Empty;
            strSQL = "SELECT username, password FROM wb_user " +
            "WHERE user_delete = 'False' AND email = '" + email + "' ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }
    }
}