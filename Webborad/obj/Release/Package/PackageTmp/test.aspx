﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="Webborad.test" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-8 border">.col-xs-12 .col-sm-6 .col-md-8</div>
            <div class="col-xs-6 col-md-4 border">.col-xs-6 .col-md-4</div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-4 border">.col-xs-6 .col-sm-4</div>
            <div class="col-xs-6 col-sm-4 border">.col-xs-6 .col-sm-4</div>
            <!-- Optional: clear the XS cols if their content doesn't match in height -->
            <div class="clearfix visible-xs-block"></div>
            <div class="col-xs-6 col-sm-4 border">.col-xs-6 .col-sm-4</div>
        </div>
    </div>
</asp:Content>
