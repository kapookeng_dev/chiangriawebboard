﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="forum_by_category.aspx.cs" Inherits="Webborad.Account_user.function.forum_by_category" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row col-md-12">
            <asp:Repeater ID="myRepeater" runat="server" OnItemDataBound="myRepeater_ItemDataBound">
                <ItemTemplate>
                    <div class="col-sm-3 col-md-2 card m-2 pr-0 pl-0">
                            <asp:Image ID="img" runat="server" Width="100%" Height="100" />
                            <asp:HyperLink ID="lbl_name" runat="server" CssClass="nav-link bg-main text-main text-center" Width="100%"></asp:HyperLink>
                        <div class="card-body pt-0 pb-0">
                            <div class="row">
                                <asp:Label ID="head_topic" CssClass="text-submain" runat="server" Text="กระทู้ : "></asp:Label>
                                <asp:Label ID="lbl_topic" CssClass="text-submain" runat="server"></asp:Label>
                            </div>
                            <div class="row">
                                <asp:Label ID="head_comment" CssClass="text-submain" runat="server" Text="คอมเมนต์ : "></asp:Label>
                                <asp:Label ID="lbl_comment" CssClass="text-submain" runat="server"></asp:Label>
                            </div>
                            <div class="row">
                                <asp:Label ID="head_last" CssClass="text-submain" runat="server" Text="ล่าสุด : "></asp:Label>
                                <asp:Label ID="lbl_last" CssClass="text-submain" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
