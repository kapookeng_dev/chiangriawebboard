﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="Webborad.Account_user.function.search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <label id="lbl_header" runat="server" class="text-submain" style="font-size : 26px"></label>
        <br />
        <br />
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-12">
                <div class="table-responsive">
                    <asp:GridView ID="myGridView" runat="server" AutoGenerateColumns="False" GridLines="Vertical" Width="100%"
                        DataKeyNames="topic_id"
                        OnRowDataBound="myGridView_RowDataBound"
                        OnPageIndexChanging="ShowPageCommand" AllowPaging="True" PageSize="5"
                        CssClass="table table-borderless border-0"
                        PagerStyle-CssClass="pagination-md" 
                        HeaderStyle-CssClass="table-bordered text-submain">
                        <Columns>
                            <asp:TemplateField HeaderText="ประเภท" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="10%" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpl_category" runat="server" CssClass="form-text"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="บทความ" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="15%" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpl_forum" runat="server" CssClass="form-text"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หัวข้อ" HeaderStyle-CssClass="text-center">
                                <ItemStyle Width="25%" CssClass="nav-link bg-submain text-submain" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpl_topic" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เริ่มโดย" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="15%" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpl_name" runat="server" CssClass="form-text"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ตอบ" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="10%" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_comment" runat="server" CssClass="form-text"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อ่าน" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="10%" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_view" runat="server" CssClass="form-text"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="คอมเมนต์ล่าสุด" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-submain text-submain" Width="15%" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_last" runat="server" CssClass="form-text"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
