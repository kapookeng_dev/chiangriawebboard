﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="topic_add.aspx.cs" Inherits="Webborad.Account_user.function.topic_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row justify-content-center">
            <div class="col-md-10 card card-body shadow">
                <fieldset>
                    <legend>Add Topic</legend>
                    <div class="form-group row">
                        <label for="txt_name" class="col-sm-3 col-form-label">Topic Name</label>
                        <div class="col-sm-9">
                            <input type="text" runat="server" class="form-control" id="txt_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="txt_detail" class="col-sm-3 col-form-label">Topic Detail</label>
                        <div class="col-sm-9">
                            <textarea class="ckeditor" runat="server" id="txt_detail"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <asp:Button ID="btn_save" runat="server" class="btn bg-main text-main" Text="Save" OnClick="btn_save_Click" />
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btn_cancel" runat="server" class="btn bg-main text-main" Text="Cancel"  />
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
