﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="view_user.aspx.cs" Inherits="Webborad.Account_user.function.view_user" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <label class="text-submain" style="font-size : 26px">โปรไฟล์ :</label>
                    <label id="lbl_name" runat="server" class="text-submain" style="font-size : 26px"></label>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="card col-sm-2 col-md-2 mr-1 text-submain align-items-center">
                    <div class="card-body">
                        <div class="row pb-1">
                            <asp:Image ID="img_user" CssClass="rounded-circle" runat="server" Width="100" Height="100" />
                        </div>
                        <div class="row">
                            <asp:Label id="lbl_user_name" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <asp:Label runat="server" Text="ระดับ : "></asp:Label>
                            <asp:Label ID="lbl_user_role" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <asp:Label runat="server" Text="จำนวนกระทู้ : "></asp:Label>
                            <asp:Label ID="lbl_user_topic" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <asp:Label runat="server" Text="จำนวนตอบกลับ : "></asp:Label>
                            <asp:Label ID="lbl_user_comment" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                
                <div class="card card-body col-sm-9 col-md-9">
                    <div class="table-responsive">
                        <asp:GridView ID="myGridView" runat="server" AutoGenerateColumns="False" GridLines="Vertical" Width="100%"
                            DataKeyNames="topic_id"
                            OnRowDataBound="myGridView_RowDataBound"
                            OnPageIndexChanging="ShowPageCommand" AllowPaging="True" PageSize="5"
                            CssClass="table table-borderless border-0"
                            PagerStyle-CssClass="pagination-md" 
                            HeaderStyle-CssClass="table-bordered text-submain">
                            <Columns>
                                <asp:TemplateField HeaderText="ประเภท" HeaderStyle-CssClass="text-center">
                                    <ItemStyle CssClass="bg-main text-main" Width="10%" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpl_category" runat="server" CssClass="form-text"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="บทความ" HeaderStyle-CssClass="text-center">
                                    <ItemStyle CssClass="bg-main text-main" Width="15%" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpl_forum" runat="server" CssClass="form-text"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หัวข้อ" HeaderStyle-CssClass="text-center">
                                    <ItemStyle Width="35%" CssClass="nav-link bg-submain text-submain" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpl_topic" runat="server"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ตอบ" HeaderStyle-CssClass="text-center">
                                    <ItemStyle CssClass="bg-main text-main" Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_comment" runat="server" CssClass="form-text"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="อ่าน" HeaderStyle-CssClass="text-center">
                                    <ItemStyle CssClass="bg-main text-main" Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_view" runat="server" CssClass="form-text"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คอมเมนต์ล่าสุด" HeaderStyle-CssClass="text-center">
                                    <ItemStyle CssClass="bg-submain text-submain" Width="20%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_last" runat="server" CssClass="form-text"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
