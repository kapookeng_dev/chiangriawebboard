﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Webborad.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row justify-content-center">
            <div class="col-md-3 card card-body shadow">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title text-submain">เข้าสู่ระบบ</h2>
                        <br />
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <asp:TextBox ID="txtuser" runat="server" class="form-control" placeholder="Username"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtpass" runat="server" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="form-group row justify-content-center">
                            <asp:Label ID="lblstatus" CssClass="form-text text-center" runat="server" Text="* username หรือ password ไม่ถูกต้อง" ForeColor="Red" Visible="false" Width="300"></asp:Label>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-md-5">
                                <asp:HyperLink ID="hpl_forget" class="form-text text-center text-submain" runat="server" NavigateUrl="#">Forget Password</asp:HyperLink>
                            </div>
                            <div class="col-md-5">
                                <asp:HyperLink ID="hpl_regis" class="form-text text-center text-submain" runat="server" NavigateUrl="#">Register</asp:HyperLink>
                            </div>
                        </div>
                        <asp:Button ID="btnlogin" runat="server" Text="Login" OnClick="btnlogin_Click" class="btn btn-lg btn-block bg-main text-main" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
