﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad
{
    public partial class login : System.Web.UI.Page
    {
        string strSQL = string.Empty;
        SqlCommand objCmd;
        SqlDataReader dtReader;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Response.Redirect("~/default.aspx");
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            if (clsData.Login(txtuser.Text, txtpass.Text))
            {
                clsDB.sqlserverrxcon(ref clsDB.objConn);
                strSQL = "SELECT wb_user.user_id, wb_user.show_name, wb_user_role.role_id FROM wb_user" +
                    " INNER JOIN wb_user_role ON wb_user_role.user_id = wb_user.user_id" +
                    " WHERE username = '" + this.txtuser.Text + "' " +
                    " AND password = '" + this.txtpass.Text + "' ";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                dtReader = objCmd.ExecuteReader();
                while (dtReader.Read())
                {
                    Session["user"] = dtReader["user_id"].ToString();
                    Session["show"] = dtReader["show_name"].ToString();
                    Session["role"] = dtReader["role_id"].ToString();
                }
                if (Session["role"].ToString() == "2")
                    Response.Redirect("~/default.aspx");
                else
                    Response.Redirect("~/Account_admin/main_manage.aspx");
            }
            else
            {
                Session.Clear();
                this.txtpass.Text = null;
                this.lblstatus.Visible = true;
            }
        }
    }
}