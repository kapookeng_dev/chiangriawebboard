﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account
{
    public partial class forget_password : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = clsData.Forget_Pass(txt_mail.Value);
            if (dt != null && dt.Rows.Count > 0)
            {
                string MailBody = "<h2>Webborad</h2>";

                MailBody += "<h3>username : " + dt.Rows[0]["username"] + "</h3>";
                MailBody += "<h3>password : " + dt.Rows[0]["password"] + "</h3>";

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("Webboard@gmail.com");
                mail.To.Add(new MailAddress(txt_mail.Value));
                mail.Subject = "รหัสผ่าน Webborad";
                mail.Body = MailBody;
                mail.IsBodyHtml = true;
                using (SmtpClient smtp = new SmtpClient("smtp-relay.gmail.com", 25))
                {
                    smtp.Send(mail);
                }
            }
            else
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showError", "alert('ไม่พบบัญชีที่ใช้อีเมลล์นี้');", true);
        }
    }
}