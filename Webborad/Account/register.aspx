﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Webborad.Account.register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row justify-content-center">
            <div class="col-md-5 card card-body shadow">
                <fieldset>
                    <legend>Register</legend>
                    <div class="form-group row">
                        <label for="txt_show" class="col-sm-3 col-form-label">Show Name</label>
                        <div class="col-sm-9">
                            <input type="text" runat="server" class="form-control" id="txt_show">
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_user">Username</label>
                                <input type="text" runat="server" class="form-control" id="txt_user">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_pass">Password</label>
                                <input type="text" runat="server" class="form-control" id="txt_pass">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_first">Firstname</label>
                                <input type="text" runat="server" class="form-control" id="txt_first">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_sur">Surname</label>
                                <input type="text" runat="server" class="form-control" id="txt_sur">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-sm row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ddl_gender">Gender</label>
                                <asp:DropDownList ID="ddl_gender" runat="server" CssClass="selectpicker form-control" DataSourceID="SqlDtGender"
                                    DataTextField="gender_name" DataValueField="gender_id"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_mail">E-Mail</label>
                                <input type="text" runat="server" class="form-control" id="txt_mail">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txt_tel">Tel</label>
                                <input type="text" runat="server" class="form-control" id="txt_tel">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="txt_address" class="col-sm-3 col-form-label">Address</label>
                        <div class="col-sm-9">
                            <input type="text" runat="server" class="form-control" id="txt_address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <asp:Button ID="btn_save" runat="server" class="btn bg-main text-main" Text="Save" OnClick="btn_save_Click" />
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btn_cancel" runat="server" class="btn bg-main text-main" Text="Cancel" PostBackUrl="~/default.aspx" />
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <asp:SqlDataSource ID="SqlDtGender" runat="server" ConnectionString="<%$ ConnectionStrings:connDB %>" SelectCommand="SELECT * FROM [wb_gender]"></asp:SqlDataSource>
</asp:Content>
