﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="forget_password.aspx.cs" Inherits="Webborad.Account.forget_password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row justify-content-center">
            <div class="col-md-5 card card-body shadow">
                <fieldset>
                    <legend>Forget Password</legend>
                    <div class="form-group row">
                        <label for="txt_mail" class="col-sm-3 col-form-label">กรอกอีเมล์</label>
                        <div class="col-sm-9">
                            <input type="text" runat="server" class="form-control" id="txt_mail">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <asp:Button ID="btn_submit" runat="server" class="btn bg-main text-main" Text="Save" OnClick="btn_submit_Click" />
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btn_cancel" runat="server" class="btn bg-main text-main" Text="Cancel" PostBackUrl="~/default.aspx" />
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
