﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["user"] == null)
            //{
            //    Session.Abandon();
            //    Response.Redirect("~/default.aspx");
            //}

            clsDB.sqlserverrxcon(ref clsDB.objConn);
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            string strSQL = string.Empty;
            int UserID = 0;
            int RoleID = 0;
            SqlCommand objCmd;
            try
            {
                //** Insert New User **//
                strSQL = "INSERT INTO wb_user (username, password, show_name, firstname, surname, gender_id, email, tel, address) " +
                        " VALUES " +
                        " (@user, @pass, @show, @first, @sur, @gender, @mail, @tel, @address)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@user", SqlDbType.NVarChar).Value = txt_user.Value;
                objCmd.Parameters.Add("@pass", SqlDbType.NVarChar).Value = txt_pass.Value;
                objCmd.Parameters.Add("@show", SqlDbType.NVarChar).Value = txt_show.Value;
                objCmd.Parameters.Add("@first", SqlDbType.NVarChar).Value = txt_first.Value;
                objCmd.Parameters.Add("@sur", SqlDbType.NVarChar).Value = txt_sur.Value;
                objCmd.Parameters.Add("@gender", SqlDbType.Int).Value = ddl_gender.SelectedValue;
                objCmd.Parameters.Add("@mail", SqlDbType.NVarChar).Value = txt_mail.Value;
                objCmd.Parameters.Add("@tel", SqlDbType.NVarChar).Value = txt_tel.Value;
                objCmd.Parameters.Add("@address", SqlDbType.NVarChar).Value = txt_address.Value;
                objCmd.ExecuteNonQuery();

                //*** Select User ID ***//
                strSQL = "SELECT Max(user_id) As UserID FROM wb_user";
                DataTable dtUser = clsData.DtTable(strSQL);
                if (dtUser.Rows.Count > 0)
                {
                    UserID = Convert.ToInt32(dtUser.Rows[0]["UserID"]);
                }

                //*** Select Role ID ***//
                strSQL = "SELECT role_id As RoleID FROM wb_role WHERE role_name = 'user' ";
                DataTable dtRole = clsData.DtTable(strSQL);
                if (dtRole.Rows.Count > 0)
                {
                    RoleID = Convert.ToInt32(dtRole.Rows[0]["RoleID"]);
                }

                //** Insert New User Role **//
                strSQL = "INSERT INTO wb_user_role (user_id, role_id) " +
                        " VALUES " +
                        " (@user, @role)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@user", SqlDbType.Int).Value = UserID;
                objCmd.Parameters.Add("@role", SqlDbType.Int).Value = RoleID;
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            //** Insert Log **//
            try
            {
                clsData.SaveLog("user", "ADD", UserID, UserID);

                clsDB.objConn.Close();
                Response.Write("<script language='javascript'>window.alert('สมัครเรียบร้อย');window.location='login.aspx';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}