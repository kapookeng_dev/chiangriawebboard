﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Webborad
{
    public class clsDB
    {
        public static SqlConnection objConn = new SqlConnection();

        private static string SQLserverrx()
        {
            return WebConfigurationManager.ConnectionStrings["connDB"].ConnectionString;
        }

        public static bool sqlserverrxcon(ref SqlConnection cn)
        {
            bool flag = false;
            clsDB.closesqlcon(ref cn);
            try
            {
                if (cn.State == ConnectionState.Closed)
                {
                    cn.ConnectionString = clsDB.SQLserverrx();
                    cn.Open();
                    flag = true;
                }
            }
            catch
            {
                flag = false;
            }
            return flag;
        }

        public static bool closesqlcon(ref SqlConnection cn)
        {
            bool flag = false;
            try
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                    flag = true;
                }
            }
            catch
            {
                flag = false;
            }
            return flag;
        }

        public static bool DBNullBoolean(object ob)
        {
            return ob == DBNull.Value ? false : Convert.ToBoolean(ob);
        }

        public static string DBNullString(object ob)
        {
            return ob == DBNull.Value ? " " : Convert.ToString(ob);
        }

        public static int DBNullInt(object ob)
        {
            return ob == DBNull.Value ? 0 : Convert.ToInt32(ob);
        }

        public static double DBNullDouble(object ob)
        {
            return ob == DBNull.Value ? 0 : Convert.ToDouble(ob);
        }

        public static DateTime DBNullDatetime(object ob)
        {
            return ob == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(ob);
        }
    }
}