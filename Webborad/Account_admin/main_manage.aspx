﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_admin.Master" AutoEventWireup="true" CodeBehind="main_manage.aspx.cs" Inherits="Webborad.Account_admin.main_manage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row justify-content-center">
            <div class="col-md-5 card card-body shadow">
                <fieldset>
                    <legend>Manage</legend>
                    <%--<div class="form-group row offset-md-1">
                        <asp:HyperLink ID="hpl_category" runat="server" CssClass="nav-link bg-main text-main" NavigateUrl="~/Account_admin/manage/category.aspx">Category</asp:HyperLink>
                    </div>--%>
                    <div class="form-group row offset-md-1">
                        <asp:HyperLink ID="hpl_forum" runat="server" CssClass="nav-link bg-main text-main" NavigateUrl="~/Account_admin/manage/forum.aspx">Forum</asp:HyperLink>
                    </div>
                    <div class="form-group row offset-md-1">
                        <asp:HyperLink ID="hpl_topic" runat="server" CssClass="nav-link bg-main text-main" NavigateUrl="~/Account_admin/manage/topic.aspx">Topic</asp:HyperLink>
                    </div>
                    <div class="form-group row offset-md-1">
                        <asp:HyperLink ID="hpl_comment" runat="server" CssClass="nav-link bg-main text-main" NavigateUrl="~/Account_admin/manage/comment.aspx">Comment</asp:HyperLink>
                    </div>
                    <div class="form-group row offset-md-1">
                        <asp:HyperLink ID="hpl_member" runat="server" CssClass="nav-link bg-main text-main" NavigateUrl="~/Account_admin/manage/user.aspx">Member</asp:HyperLink>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
