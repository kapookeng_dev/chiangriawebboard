﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_admin.manage
{
    public partial class topic : System.Web.UI.Page
    {
        String strSQL = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["show"] == null)
            //{
            //    Session.Abandon();
            //    Response.Redirect("~/default.aspx");
            //}
            clsDB.sqlserverrxcon(ref clsDB.objConn);
            if (!Page.IsPostBack)
            {

            }
        }

        protected void BindData()
        {
            DataTable dt;
            dt = clsData.getTopicInForum(Convert.ToInt32(Session["forum"]));

            //*** BindData to GridView ***//
            if (dt != null && dt.Rows.Count > 0)
            {
                myGridView.DataSource = dt;
                myGridView.DataBind();
            }
            else
            {
                myGridView.DataSource = null;
                myGridView.DataBind();
            }
        }

        protected void ShowPageCommand(Object s, GridViewPageEventArgs e)
        {
            myGridView.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void myGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                if (e.CommandName == "open_status")
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    var getid = myGridView.Rows[RowIndex].FindControl("hid_id") as HiddenField;
                    int getid_value = Convert.ToInt32(getid.Value);

                    strSQL = "UPDATE wb_topic SET topic_status = @status " +
                                  " WHERE topic_id = " + getid_value + "";
                    objCmd = new SqlCommand(strSQL, clsDB.objConn);
                    objCmd.Parameters.Add("@status", System.Data.SqlDbType.Bit).Value = true;
                    objCmd.ExecuteNonQuery();

                    //** Insert log**//
                    clsData.SaveLog("topic", "EDIT STATUS", 1, getid_value);
                }

                if (e.CommandName == "close_status")
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    var getid = myGridView.Rows[RowIndex].FindControl("hid_id") as HiddenField;
                    int getid_value = Convert.ToInt32(getid.Value);

                    strSQL = "UPDATE wb_topic SET topic_status = @status " +
                                  " WHERE topic_id = " + getid_value + "";
                    objCmd = new SqlCommand(strSQL, clsDB.objConn);
                    objCmd.Parameters.Add("@status", System.Data.SqlDbType.Bit).Value = false;
                    objCmd.ExecuteNonQuery();

                    //** Insert log**//
                    clsData.SaveLog("topic", "EDIT STATUS", 1, getid_value);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            BindData();
        }

        protected void modDeleteCommand(Object sender, GridViewDeleteEventArgs e)
        {
            string table = "topic";
            int id = Convert.ToInt32(myGridView.DataKeys[e.RowIndex].Value);

            clsData.DeleteDataByID(table, id);

            myGridView.EditIndex = -1;
            BindData();

        }

        protected void modUpdateCommand(Object sender, GridViewUpdateEventArgs e)
        {
            string id = myGridView.DataKeys[e.RowIndex].Value.ToString();
            Response.Redirect("~/Account_admin/manage/topic_edit.aspx?topic_id=" + id);
        }

        protected void myGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hid_id = (HiddenField)(e.Row.FindControl("hid_id"));
                if (hid_id != null)
                {
                    hid_id.Value = (string)DataBinder.Eval(e.Row.DataItem, "topic_id").ToString();
                }

                int topic_id = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "topic_id"));

                Label lbl_topic = (Label)(e.Row.FindControl("lbl_topic"));
                if (lbl_topic != null)
                {
                    lbl_topic.Text = (string)DataBinder.Eval(e.Row.DataItem, "topic_name").ToString();
                }

                Label lbl_name = (Label)(e.Row.FindControl("lbl_name"));
                if (lbl_name != null)
                {
                    lbl_name.Text = clsData.getUserShowName(Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "user_id")));
                }

                Label lbl_comment = (Label)(e.Row.FindControl("lbl_comment"));
                if (lbl_comment != null)
                {
                    lbl_comment.Text = (string)DataBinder.Eval(e.Row.DataItem, "topic_comment").ToString();
                }

                Label lbl_view = (Label)(e.Row.FindControl("lbl_view"));
                if (lbl_view != null)
                {
                    lbl_view.Text = (string)DataBinder.Eval(e.Row.DataItem, "topic_view").ToString();
                }

                LinkButton lnkbtn_close = (LinkButton)e.Row.FindControl("lnkbtn_close");
                LinkButton lnkbtn_open = (LinkButton)e.Row.FindControl("lnkbtn_open");
                if (lnkbtn_open != null)
                {
                    if (DataBinder.Eval(e.Row.DataItem, "topic_status").ToString() == "False")
                    {
                        lnkbtn_open.Visible = true;
                        lnkbtn_close.Visible = false;
                    }
                    else
                    {
                        lnkbtn_open.Visible = false;
                        lnkbtn_close.Visible = true;
                    }
                }
            }
        }

        protected void ddl_category_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["category"] = ddl_category.SelectedValue;
            DataTable dt2 = new DataTable();
            dt2 = clsData.getForumInCategory(Convert.ToInt32(Session["category"]));
            ddl_forum.DataSource = dt2;
            ddl_forum.DataTextField = "forum_name";
            ddl_forum.DataValueField = "forum_id";
            ddl_forum.DataBind();
        }

        protected void ddl_forum_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["forum"] = ddl_forum.SelectedValue;
            BindData();
            hpl_add.NavigateUrl = "~/Account_admin/manage/topic_add.aspx?category_id=" + Session["category"].ToString() + "&forum_id=" + Session["forum"].ToString();
        }
    }
}