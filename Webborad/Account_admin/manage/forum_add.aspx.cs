﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_admin.manage
{
    public partial class forum_add : System.Web.UI.Page
    {
        protected int category_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["category_id"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["user"] == null)
            //{
            //    Session.Abandon();
            //    Response.Redirect("~/default.aspx");
            //}

            clsDB.sqlserverrxcon(ref clsDB.objConn);
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            string strSQL = string.Empty;
            int ID = 0;
            SqlCommand objCmd;
            try
            {
                //** Insert New User **//
                strSQL = "INSERT INTO wb_forum (forum_name, category_id, forum_status, forum_create_by) " +
                        " VALUES " +
                        " (@forum, @category, @status, @by)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@forum", SqlDbType.NVarChar).Value = txt_name.Value;
                objCmd.Parameters.Add("@category", SqlDbType.Int).Value = category_id;
                objCmd.Parameters.Add("@status", SqlDbType.Bit).Value = false;
                objCmd.Parameters.Add("@by", SqlDbType.Int).Value = 1;
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            //** Insert Log **//
            try
            {
                //*** Select User ID ***//
                strSQL = "SELECT Max(forum_id) As ID FROM wb_forum";
                DataTable dtCategory = clsData.DtTable(strSQL);
                if (dtCategory.Rows.Count > 0)
                {
                    ID = Convert.ToInt32(dtCategory.Rows[0]["ID"]);
                }

                clsData.SaveLog("forum", "ADD", 1, ID);

                clsDB.objConn.Close();
                Response.Write("<script language='javascript'>window.alert('เพิ่มเรียบร้อย');window.location='~/Account_admin/manage/forum.aspx';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}