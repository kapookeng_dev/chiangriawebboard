﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_admin.manage
{
    public partial class forum : System.Web.UI.Page
    {
        String strSQL = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["show"] == null)
            //{
            //    Session.Abandon();
            //    Response.Redirect("~/default.aspx");
            //}
            clsDB.sqlserverrxcon(ref clsDB.objConn);
            if (!Page.IsPostBack)
            {

            }
        }

        protected void BindData()
        {
            DataTable dt;
            dt = clsData.getForumInCategory(Convert.ToInt32(Session["category"]));

            //*** BindData to GridView ***//
            if (dt != null && dt.Rows.Count > 0)
            {
                myGridView.DataSource = dt;
                myGridView.DataBind();
            }
            else
            {
                myGridView.DataSource = null;
                myGridView.DataBind();
            }
        }

        protected void ShowPageCommand(Object s, GridViewPageEventArgs e)
        {
            myGridView.PageIndex = e.NewPageIndex;
            BindData();
        }

        protected void myGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                if (e.CommandName == "open_status")
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    var getid = myGridView.Rows[RowIndex].FindControl("hid_id") as HiddenField;
                    int getid_value = Convert.ToInt32(getid.Value);

                    strSQL = "UPDATE wb_forum SET forum_status = @status " +
                                  " WHERE forum_id = " + getid_value + "";
                    objCmd = new SqlCommand(strSQL, clsDB.objConn);
                    objCmd.Parameters.Add("@status", System.Data.SqlDbType.Bit).Value = true;
                    objCmd.ExecuteNonQuery();

                    //** Insert log**//
                    clsData.SaveLog("forum", "EDIT STATUS", 1, getid_value);
                }

                if (e.CommandName == "close_status")
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    var getid = myGridView.Rows[RowIndex].FindControl("hid_id") as HiddenField;
                    int getid_value = Convert.ToInt32(getid.Value);

                    strSQL = "UPDATE wb_forum SET forum_status = @status " +
                                  " WHERE forum_id = " + getid_value + "";
                    objCmd = new SqlCommand(strSQL, clsDB.objConn);
                    objCmd.Parameters.Add("@status", System.Data.SqlDbType.Bit).Value = false;
                    objCmd.ExecuteNonQuery();

                    //** Insert log**//
                    clsData.SaveLog("forum", "EDIT STATUS", 1, getid_value);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            BindData();
        }

        protected void modDeleteCommand(Object sender, GridViewDeleteEventArgs e)
        {
            string table = "forum";
            int id = Convert.ToInt32(myGridView.DataKeys[e.RowIndex].Value);

            clsData.DeleteDataByID(table, id);

            myGridView.EditIndex = -1;
            BindData();

        }

        protected void modUpdateCommand(Object sender, GridViewUpdateEventArgs e)
        {
            string id = myGridView.DataKeys[e.RowIndex].Value.ToString();
            Response.Redirect("edit_forum.aspx?forum_id=" + id);
        }

        protected void myGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hid_id = (HiddenField)(e.Row.FindControl("hid_id"));
                if (hid_id != null)
                {
                    hid_id.Value = (string)DataBinder.Eval(e.Row.DataItem, "forum_id").ToString();
                }

                int forum_id = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "forum_id"));

                Label lbl_forum = (Label)(e.Row.FindControl("lbl_forum"));
                if (lbl_forum != null)
                {
                    lbl_forum.Text = (string)DataBinder.Eval(e.Row.DataItem, "forum_name").ToString();
                }

                Label lbl_topic = (Label)(e.Row.FindControl("lbl_topic"));
                if (lbl_topic != null)
                {
                    lbl_topic.Text = clsDataCount.CountTopicInForum(forum_id).ToString();
                }

                Label lbl_view = (Label)(e.Row.FindControl("lbl_view"));
                if (lbl_view != null)
                {
                    lbl_view.Text = clsDataCount.CountViewInForum(forum_id).ToString();
                }

                LinkButton lnkbtn_close = (LinkButton)e.Row.FindControl("lnkbtn_close");
                LinkButton lnkbtn_open = (LinkButton)e.Row.FindControl("lnkbtn_open");
                if (lnkbtn_open != null)
                {
                    if (DataBinder.Eval(e.Row.DataItem, "forum_status").ToString() == "False")
                    {
                        lnkbtn_open.Visible = true;
                        lnkbtn_close.Visible = false;
                    }
                    else
                    {
                        lnkbtn_open.Visible = false;
                        lnkbtn_close.Visible = true;
                    }
                }
            }
        }

        protected void ddl_category_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["category"] = ddl_category.SelectedValue;
            BindData();
            hpl_add.NavigateUrl = "~/Account_admin/manage/forum_add.aspx?category_id=" + Session["category"].ToString();
        }
    }
}