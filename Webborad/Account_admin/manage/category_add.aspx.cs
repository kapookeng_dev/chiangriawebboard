﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_admin.manage
{
    public partial class category_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["user"] == null)
            //{
            //    Session.Abandon();
            //    Response.Redirect("~/default.aspx");
            //}

            clsDB.sqlserverrxcon(ref clsDB.objConn);
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            string strSQL = string.Empty;
            int ID = 0;
            SqlCommand objCmd;
            try
            {
                //** Insert New User **//
                strSQL = "INSERT INTO wb_category (category_name, category_status, category_create_by) " +
                        " VALUES " +
                        " (@category, @status, @by)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@category", SqlDbType.NVarChar).Value = txt_name.Value;
                objCmd.Parameters.Add("@status", SqlDbType.Bit).Value = false;
                objCmd.Parameters.Add("@by", SqlDbType.Int).Value = 1;
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            //** Insert Log **//
            try
            {
                //*** Select User ID ***//
                strSQL = "SELECT Max(category_id) As ID FROM wb_category";
                DataTable dtCategory = clsData.DtTable(strSQL);
                if (dtCategory.Rows.Count > 0)
                {
                    ID = Convert.ToInt32(dtCategory.Rows[0]["ID"]);
                }

                clsData.SaveLog("category", "ADD", 1, ID);

                clsDB.objConn.Close();
                Response.Write("<script language='javascript'>window.alert('เพิ่มเรียบร้อย');window.location='manage_category.aspx';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}