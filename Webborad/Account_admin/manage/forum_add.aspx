﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_admin.Master" AutoEventWireup="true" CodeBehind="forum_add.aspx.cs" Inherits="Webborad.Account_admin.manage.forum_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="row justify-content-center">
            <div class="col-md-5 card card-body shadow">
                <fieldset>
                    <legend>เพิ่มฟอรัม</legend>
                    <div class="form-group row">
                        <label for="txt_name" class="col-sm-3 col-form-label">ชื่อฟอรัม</label>
                        <div class="col-sm-9">
                            <input type="text" runat="server" class="form-control" id="txt_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <asp:Button ID="btn_save" runat="server" class="btn bg-main text-main" Text="Save" OnClick="btn_save_Click" />
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btn_cancel" runat="server" class="btn bg-main text-main" Text="Cancel" PostBackUrl="~/Account_admin/manage/forum.aspx" />
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</asp:Content>
