﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_admin.Master" AutoEventWireup="true" CodeBehind="topic.aspx.cs" Inherits="Webborad.Account_admin.manage.topic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
        <div class="container_main">
        <asp:DropDownList ID="ddl_category" runat="server" CssClass="selectpicker" OnSelectedIndexChanged="ddl_category_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="เลือกประเภท"  Selected="True"></asp:ListItem>
            <asp:ListItem Text="ประเภทที่1" Value="1"></asp:ListItem>
            <asp:ListItem Text="ประเภทที่2" Value="2"></asp:ListItem>
            <asp:ListItem Text="ประเภทที่3" Value="3"></asp:ListItem>
            <asp:ListItem Text="ประเภทที่4" Value="4"></asp:ListItem>
            <asp:ListItem Text="ประเภทที่5" Value="5"></asp:ListItem>
            <asp:ListItem Text="ประเภทที่6" Value="6"></asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:DropDownList ID="ddl_forum" runat="server" CssClass="selectpicker" OnSelectedIndexChanged="ddl_forum_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
        <br />
        <br />
        <asp:HyperLink ID="hpl_add" runat="server" CssClass="btn bg-main text-main">เพิ่มกระทู้</asp:HyperLink>
        <br />
        <br />
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="table-responsive">
                    <asp:GridView ID="myGridView" runat="server" AutoGenerateColumns="False" GridLines="Vertical" Width="100%"
                        DataKeyNames="topic_id"
                        OnRowDeleting="modDeleteCommand"
                        OnRowUpdating="modUpdateCommand"
                        OnRowCommand="myGridView_RowCommand"
                        OnRowDataBound="myGridView_RowDataBound"
                        OnPageIndexChanging="ShowPageCommand" AllowPaging="True" PageSize="5"
                        CssClass="table table-borderless border-0 table-striped" PagerStyle-CssClass="pagination-md">
                        <Columns>
                            <asp:TemplateField ShowHeader="false">
                                <ItemStyle Width="40%" CssClass="nav-link bg-submain text-submain"/>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hid_id" runat="server" />
                                    <asp:Label ID="lbl_topic" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เริ่มโดย" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="20%" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_name" runat="server" CssClass="form-text"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ตอบ" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="10%" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_comment" runat="server" CssClass="form-text"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อ่าน" HeaderStyle-CssClass="text-center">
                                <ItemStyle CssClass="bg-main text-main" Width="10%" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lbl_view" runat="server" CssClass="form-text"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ShowHeader="False">
                                <ItemStyle CssClass="bg-submain text-submain" Width="20%" HorizontalAlign="Center"/>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtn_edit" runat="server" CssClass="btn btn-sm bg-main text-main" CausesValidation="True" CommandName="Update" Text="แก้ไข"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="lnkbtn_open" runat="server" CssClass="btn btn-sm bg-main text-main" CommandName="open_status" Text="Open" OnClientClick="return confirm('ยืนยัน ?');"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_close" runat="server" CssClass="btn btn-sm bg-main text-main" CommandName="close_status" Text="Close" OnClientClick="return confirm('ยืนยัน ?');"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="lnkbtn_delete" runat="server" CssClass="btn btn-sm bg-main text-main" CausesValidation="False" CommandName="Delete" Text="ลบ" OnClientClick="return confirm('ยืนยัน ?');"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
