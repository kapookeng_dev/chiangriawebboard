﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad
{
    public partial class master_admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                pnl_login.Visible = false;
                pnl_logout.Visible = true;
                lbl_name.InnerText = Session["show"].ToString();
                img_user.Src = "~/pic_user/" + clsData.getUserImg(Convert.ToInt32(Session["user"]));
            }
            else
                pnl_logout.Visible = false;

            //link_search.HRef = "~/Account_user/function/search.aspx";

            txt_all.InnerText = clsDataCount.AllLogin().ToString();
            txt_now.InnerText = clsDataCount.CurrentLogin().ToString();
            txt_last.InnerText = clsDataCount.LastDayLogin().ToString();
            txt_week.InnerText = clsDataCount.WeekLogin().ToString();
            txt_lastweek.InnerText = clsDataCount.LastWeekLogin().ToString();
            txt_month.InnerText = clsDataCount.MonthLogin().ToString();
            txt_lastmonth.InnerText = clsDataCount.LastMonthLogin().ToString();
        }
    }
}