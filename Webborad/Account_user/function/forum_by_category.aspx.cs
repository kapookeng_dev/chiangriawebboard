﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_user.function
{
    public partial class forum_by_category : System.Web.UI.Page
    {
        protected int category_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["category_id"]);

        string strSQL = string.Empty;
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["show"] == null)
            {
                //Session.Abandon();
                //Response.Redirect("~/default.aspx");
            }

            clsDB.sqlserverrxcon(ref clsDB.objConn);

            if (!Page.IsPostBack)
            {
                BindData();
                clsDataCount.Log_Login();
            }
        }

        protected void BindData()
        {
            dt = clsData.getForumInCategory(category_id);

            //*** BindData to Repeater ***//
            if (dt != null && dt.Rows.Count > 0)
            {
                myRepeater.DataSource = dt;
                myRepeater.DataBind();
            }
            else
            {
                myRepeater.DataSource = null;
                myRepeater.DataBind();
            }
        }

        protected void myRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int forum_id = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "forum_id"));

                Image imgItem = (Image)(e.Item.FindControl("img"));
                if (imgItem != null)
                {
                    imgItem.ImageUrl = "~/pic_forum/" + DataBinder.Eval(e.Item.DataItem, "forum_icon").ToString();
                }

                HyperLink lbl_name = (HyperLink)e.Item.FindControl("lbl_name");
                if ((lbl_name != null))
                {
                    lbl_name.Text = (string)DataBinder.Eval(e.Item.DataItem, "forum_name");
                    lbl_name.NavigateUrl = "../../Account_user/function/topic.aspx?category_id=" + category_id + "&forum_id=" + forum_id;
                }

                Label lbl_topic = (Label)e.Item.FindControl("lbl_topic");
                if ((lbl_topic != null))
                {
                    lbl_topic.Text = clsDataCount.CountTopicInForum(forum_id).ToString();
                }

                Label lbl_comment = (Label)e.Item.FindControl("lbl_comment");
                if ((lbl_comment != null))
                {
                    lbl_comment.Text = clsDataCount.CountCommentInForum(forum_id).ToString();
                }

                Label lbl_last = (Label)e.Item.FindControl("lbl_last");
                if ((lbl_last != null))
                {
                    DataTable dt2 = new DataTable();
                    dt2 = clsDataCount.LastComment(forum_id);

                    if (dt2 != null && dt2.Rows.Count > 0)
                    {
                        string time = Convert.ToDateTime(dt2.Rows[0]["comment_create"]).ToString("HH:mm:ss");
                        string name = clsData.getUserShowName(Convert.ToInt32(dt2.Rows[0]["user_id"]));
                        lbl_last.Text = name + " เวลา " + time;
                    }
                    else
                    {
                        lbl_last.Text = "ไม่มี";
                    }
                }
            }
        }
    }
}