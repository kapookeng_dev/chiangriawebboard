﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_user.function
{
    public partial class view_user : System.Web.UI.Page
    {
        protected int user_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["user_id"]);

        String strSQL = string.Empty;
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["show"] == null)
            //{
            //    Session.Abandon();
            //    Response.Redirect("~/default.aspx");
            //}
            clsDB.sqlserverrxcon(ref clsDB.objConn);
            if (!Page.IsPostBack)
            {
                BindData();
                BindDataUser();
            }
        }

        protected void BindDataUser()
        {
            img_user.ImageUrl = "~/pic_user/" + clsData.getUserImg(user_id);
            lbl_user_name.Text = clsData.getUserShowName(user_id).ToString();
            lbl_user_role.Text = clsData.getRoleByUserID(user_id);
            lbl_user_topic.Text = clsDataCount.CountTopicByUser(user_id).ToString();
            lbl_user_comment.Text = clsDataCount.CountCommentByUser(user_id).ToString();
        }

        protected void BindData()
        {
            dt = clsData.getTopicByUserID(user_id);

            //*** BindData to GridView ***//
            if (dt != null && dt.Rows.Count > 0)
            {
                myGridView.DataSource = dt;
                myGridView.DataBind();
            }
            else
            {
                myGridView.DataSource = null;
                myGridView.DataBind();
            }
        }

        protected void ShowPageCommand(Object s, GridViewPageEventArgs e)
        {
            myGridView.PageIndex = e.NewPageIndex;
            BindData();
            BindDataUser();
        }

        protected void myGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                int category_id = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "category_id"));
                int forum_id = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "forum_id"));
                int topic_id = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "topic_id"));

                HyperLink hpl_category = (HyperLink)e.Row.FindControl("hpl_category");
                if ((hpl_category != null))
                {
                    hpl_category.Text = clsData.getCategoryName(category_id);
                    hpl_category.NavigateUrl = "~/Account_user/function/forum_by_category.aspx?category_id=" + category_id;
                }

                HyperLink hpl_forum = (HyperLink)e.Row.FindControl("hpl_forum");
                if ((hpl_forum != null))
                {
                    hpl_forum.Text = clsData.getForumName(forum_id);
                    hpl_forum.NavigateUrl = "~/Account_user/function/topic.aspx?category_id=" + category_id + "&forum_id=" + forum_id;
                }

                HyperLink hpl_topic = (HyperLink)e.Row.FindControl("hpl_topic");
                if ((hpl_topic != null))
                {
                    hpl_topic.Text = (string)DataBinder.Eval(e.Row.DataItem, "topic_name");
                    hpl_topic.NavigateUrl = "~/Account_user/function/comment.aspx?category_id=" + category_id + "&topic_id=" + topic_id + "&forum_id=" + forum_id;
                }


                Label lbl_view = (Label)(e.Row.FindControl("lbl_view"));
                if (lbl_view != null)
                {
                    lbl_view.Text = (string)DataBinder.Eval(e.Row.DataItem, "topic_view").ToString();
                }

                Label lbl_comment = (Label)(e.Row.FindControl("lbl_comment"));
                if (lbl_comment != null)
                {
                    lbl_comment.Text = (string)DataBinder.Eval(e.Row.DataItem, "topic_comment").ToString();
                }

                Label lbl_last = (Label)(e.Row.FindControl("lbl_last"));
                if (lbl_last != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = clsDataCount.LastCommentTopic(topic_id);

                    if (dt2 != null && dt2.Rows.Count > 0)
                    {
                        string time = Convert.ToDateTime(dt2.Rows[0]["comment_create"]).ToString("HH:mm:ss");
                        string name = clsData.getUserShowName(Convert.ToInt32(dt2.Rows[0]["user_id"]));
                        lbl_last.Text = name + " เวลา " + time;
                    }
                    else
                    {
                        lbl_last.Text = "ไม่มี";
                    }
                }
            }
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {
            Session["count"] = 1;
        }
    }
}