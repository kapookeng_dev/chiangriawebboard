﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master_user.Master" AutoEventWireup="true" CodeBehind="comment.aspx.cs" Inherits="Webborad.Account_user.function.comment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main_Content" runat="server">
    <div class="container_main">
        <div class="col-sm-12 col-md-12">
            <%--Header--%>
            <div class="row">
                <div class="col-md-10">
                    <a id="lbl_forum" runat="server" class="text-submain" style="font-size : 26px"></a>
                    <a class="text-submain" style="font-size : 26px">></a>
                    <label id="lbl_topic_name" runat="server" class="text-submain" style="font-size : 26px"></label>
                </div>
                <div class="col-md-1 offset-1">
                    <asp:HyperLink ID="hplBack" CssClass="btn bg-main text-main" runat="server">ย้อนกลับ</asp:HyperLink>
                </div>
            </div>

            <%--Topic--%>
            <div class="row justify-content-center">
                <div class="card col-md-2 mr-1 text-submain align-items-center">
                    <div class="card-body">
                        <div class="row pb-1">
                            <asp:Image ID="img_user" CssClass="rounded-circle" runat="server" Width="100" Height="100" />
                        </div>
                        <div class="row">
                            <asp:HyperLink id="lbl_topic_user" runat="server"></asp:HyperLink>
                        </div>
                        <div class="row">
                            <asp:Label ID="lbl_topic_head" runat="server" Text="ระดับ : "></asp:Label>
                            <asp:Label ID="lbl_topic_role" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <asp:Label ID="lbl_topic_head2" runat="server" Text="จำนวนกระทู้ : "></asp:Label>
                            <asp:Label ID="lbl_topic_sum" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="card col-md-9">
                    <div class="row bg-main text-main pl-1">
                        <div class="form-group-sm">
                            <asp:Label ID="lbl_topic_name_sub" runat="server"></asp:Label><br />
                            <asp:Label ID="lbl_topic_date" runat="server"></asp:Label><br />
                            (<asp:Label ID="lbl_topic_view" runat="server"></asp:Label>)
                        </div>
                    </div>
                    <div class="card-body pl-0">
                        <asp:Label ID="lbl_topic_detail" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <br />

            <%--Comment--%>
            <div class="row justify-content-center">
                <asp:Repeater ID="myRepeater" runat="server" OnItemDataBound="myRepeater_ItemDataBound">
                    <ItemTemplate>
                        <div class="card col-md-2 mr-1 text-submain align-items-center">
                            <div class="card-body">
                                <div class="row pb-1">
                                    <asp:Image ID="img_comment_user" CssClass="rounded-circle" runat="server" Width="100" Height="100" />
                                </div>
                                <div class="row">
                                    <asp:HyperLink id="lbl_comment_user" runat="server"></asp:HyperLink>
                                </div>
                                <div class="row">
                                    <asp:Label ID="lbl_comment_head" runat="server" Text="ระดับ : "></asp:Label>
                                    <asp:Label ID="lbl_comment_role" runat="server"></asp:Label>
                                </div>
                                <div class="row">
                                    <asp:Label ID="lbl_comment_head2" runat="server" Text="จำนวนกระทู้: "></asp:Label>
                                    <asp:Label ID="lbl_comment_sum" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="card col-md-9">
                            <div class="row bg-main text-main pl-1">
                                <asp:Label ID="lbl_comment_date" runat="server"></asp:Label>
                            </div>
                            <div class="card-body pl-0">
                                <asp:Label ID="lbl_comment_detail" runat="server"></asp:Label>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <br />

            <%--New Comment--%>
            <div class="row justify-content-center">
                <asp:Panel ID="pnl_new_comment" runat="server">
                    <div class="card card-body col-md-12">
                        <div class="row">
                            <textarea class="ckeditor" runat="server" id="txt_detail"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <asp:Button ID="btn_save" runat="server" class="btn bg-main text-main" Text="Save" OnClick="btn_save_Click" />
                            </div>
                            <div class="col-md-2">
                                <asp:Button ID="btn_cancel" runat="server" class="btn bg-main text-main" Text="Cancel" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <a ID="lbl_check_login" runat="server" class="text-submain" href="~/Account/login.aspx"></a>
            </div>
        </div>
    </div>
</asp:Content>
