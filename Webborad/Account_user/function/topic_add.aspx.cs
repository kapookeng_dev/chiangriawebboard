﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_user.function
{
    public partial class topic_add : System.Web.UI.Page
    {
        protected int forum_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["forum_id"]);
        protected int category_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["category_id"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                
            }

            clsDB.sqlserverrxcon(ref clsDB.objConn);
            btn_cancel.PostBackUrl = "~/Account_user/function/topic.aspx?category_id=" + category_id + "&forum_id=" + forum_id;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            string strSQL = string.Empty;
            int ID = 0;
            SqlCommand objCmd;
            try
            {
                //** Insert New User **//
                strSQL = "INSERT INTO wb_topic (topic_name, topic_detail, category_id, forum_id, user_id) " +
                        " VALUES " +
                        " (@topic, @detail, @category, @forum, @user)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@topic", SqlDbType.NVarChar).Value = txt_name.Value;
                objCmd.Parameters.Add("@detail", SqlDbType.NVarChar).Value = txt_detail.Value;
                objCmd.Parameters.Add("@category", SqlDbType.Int).Value = category_id;
                objCmd.Parameters.Add("@forum", SqlDbType.Int).Value = forum_id;
                objCmd.Parameters.Add("@user", SqlDbType.Int).Value = Convert.ToInt32(Session["user"]);
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            //** Insert Log **//
            try
            {
                //*** Select User ID ***//
                strSQL = "SELECT Max(topic_id) As ID FROM wb_topic";
                DataTable dtCategory = clsData.DtTable(strSQL);
                if (dtCategory.Rows.Count > 0)
                {
                    ID = Convert.ToInt32(dtCategory.Rows[0]["ID"]);
                }

                clsData.SaveLog("topic", "ADD", 1, ID);

                clsDB.objConn.Close();
                Response.Write("<script language='javascript'>window.alert('เพิ่มเรียบร้อย');window.location='topic.aspx?category_id=" + category_id + "&forum_id=" + forum_id + "';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}