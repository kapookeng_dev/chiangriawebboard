﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Webborad.Account_user.function
{
    public partial class comment : System.Web.UI.Page
    {
        protected int category_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["category_id"]);
        protected int topic_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["topic_id"]);
        protected int forum_id = Convert.ToInt32(HttpContext.Current.Request.QueryString["forum_id"]);

        string strSQL = string.Empty;
        DataTable dt = new DataTable();
        SqlCommand objCmd;
        protected int strcommentNo = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                pnl_new_comment.Visible = false;
                lbl_check_login.Visible = true;
                lbl_check_login.InnerText = "กรุณาเข้าสู่ระบบ";
            }
            else
            {
                pnl_new_comment.Visible = true;
                lbl_check_login.Visible = false;
            }

            clsDB.sqlserverrxcon(ref clsDB.objConn);

            if (!Page.IsPostBack)
            {
                BindData();
                Showcomment();
            }
            hplBack.NavigateUrl = "~/Account_user/function/topic.aspx?category_id=" + category_id + "&forum_id=" + forum_id;
            lbl_forum.InnerText = clsData.getForumName(forum_id);
            lbl_forum.HRef = "~/Account_user/function/topic.aspx?category_id=" + category_id + "&forum_id=" + forum_id;
        }

        protected void BindData()
        {
            dt = clsData.getTopicByID(topic_id);
            if (dt.Rows.Count > 0)
            {
                int user_id = Convert.ToInt32(dt.Rows[0]["user_id"]);
                img_user.ImageUrl = "~/pic_user/" + clsData.getUserImg(user_id);
                lbl_topic_name.InnerText = dt.Rows[0]["topic_name"].ToString();
                lbl_topic_name_sub.Text = dt.Rows[0]["topic_name"].ToString();
                lbl_topic_detail.Text = dt.Rows[0]["topic_detail"].ToString();
                lbl_topic_user.Text = clsData.getUserShowName(user_id).ToString();
                lbl_topic_user.NavigateUrl = "~/Account_user/function/view_user.aspx?user_id=" + user_id;
                lbl_topic_date.Text = Convert.ToDateTime(dt.Rows[0]["topic_create"]).ToString("dd/MM/yyyy HH:mm");
                lbl_topic_view.Text = "อ่าน " + dt.Rows[0]["topic_view"].ToString();
                lbl_topic_sum.Text = clsDataCount.CountTopicByUser(user_id).ToString();
                lbl_topic_role.Text = clsData.getRoleByUserID(user_id).ToString();
            }

            //*** Update Number of View ***'
            if (Convert.ToInt32(Session["count"]) == 1)
            {
                strSQL = " UPDATE wb_topic SET [topic_view] = [topic_view] + 1 " +
                        " WHERE topic_id = @id ";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@id", SqlDbType.Int).Value = topic_id;
                objCmd.ExecuteNonQuery();
            }
        }

        protected void Showcomment()
        {
            dt = clsData.getCommentInTopic(topic_id);

            //*** BindData to Repeater ***//
            if (dt != null && dt.Rows.Count > 0)
            {
                myRepeater.DataSource = dt;
                myRepeater.DataBind();
            }
            else
            {
                myRepeater.DataSource = null;
                myRepeater.DataBind();
            }
        }

        protected void myRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ////*** No ***//
                //Label lbl_comment_no = (Label)e.Item.FindControl("lbl_comment_no");
                //if ((lbl_comment_no != null))
                //{
                //    lbl_comment_no.Text = "No : " + strcommentNo;
                //    strcommentNo = strcommentNo + 1;
                //}

                int id = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "user_id"));

                Image img_comment_user = (Image)(e.Item.FindControl("img_comment_user"));
                if (img_comment_user != null)
                {
                    img_comment_user.ImageUrl = "~/pic_user/" + clsData.getUserImg(id);
                }

                //*** Name ***//
                HyperLink lbl_comment_user = (HyperLink)e.Item.FindControl("lbl_comment_user");
                if ((lbl_comment_user != null))
                {
                    
                    lbl_comment_user.Text = clsData.getUserShowName(id).ToString();
                    lbl_comment_user.NavigateUrl = "~/Account_user/function/view_user.aspx?user_id=" + id;
                }

                //*** CreateDate ***//
                Label lbl_comment_date = (Label)e.Item.FindControl("lbl_comment_date");
                if ((lbl_comment_date != null))
                {
                    lbl_comment_date.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "comment_create")).ToString("dd/MM/yyyy HH:mm");
                }

                //*** Details ***//
                Label lbl_comment_detail = (Label)e.Item.FindControl("lbl_comment_detail");
                if ((lbl_comment_detail != null))
                {
                    lbl_comment_detail.Text = (string)DataBinder.Eval(e.Item.DataItem, "comment_detail");
                }

                Label lbl_comment_sum = (Label)e.Item.FindControl("lbl_comment_sum");
                if ((lbl_comment_sum != null))
                {
                    lbl_comment_sum.Text = clsDataCount.CountTopicByUser(id).ToString();
                }

                Label lbl_comment_role = (Label)e.Item.FindControl("lbl_comment_role");
                if ((lbl_comment_role != null))
                {
                    lbl_comment_role.Text = clsData.getRoleByUserID(id).ToString();
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            //*** Insert comment ***'
            strSQL = " INSERT INTO wb_comment (topic_id,user_id,comment_detail) " +
                    " VALUES " +
                    " (@topic,@user,@detail) ";
            objCmd = new SqlCommand(strSQL, clsDB.objConn);
            objCmd.Parameters.Add("@topic", SqlDbType.Int).Value = topic_id;
            objCmd.Parameters.Add("@user", SqlDbType.Int).Value = Convert.ToInt32(Session["user"]); ;
            objCmd.Parameters.Add("@detail", SqlDbType.NVarChar).Value = txt_detail.InnerText;
            objCmd.ExecuteNonQuery();

            //*** Update Number of comment ***'
            strSQL = " UPDATE wb_topic SET topic_comment = topic_comment + 1 " +
                    " WHERE topic_id = @topic ";
            objCmd = new SqlCommand(strSQL, clsDB.objConn);
            objCmd.Parameters.Add("@topic", SqlDbType.Int).Value = topic_id;
            objCmd.ExecuteNonQuery();

            Session["count"] = 0;
            Response.Redirect("../../Account_user/function/comment.aspx?topic_id=" + topic_id+ "&forum_id=" + forum_id);
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {
            Session["count"] = 0;
        }

        protected void hplBack_Load(object sender, EventArgs e)
        {
            Session.Remove("count");
        }
    }
}