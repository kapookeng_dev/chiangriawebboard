﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Webborad
{
    public class clsDataCount
    {
        private static DataTable dt = new DataTable();
        private static int count;
        private static string strSQL = string.Empty;
        //protected static string datetime_format = DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss");

        public static void Log_Login()
        {
            SqlCommand objCmd;
            string strSQL = string.Empty;

            try
            {
                //** Insert log**//
                strSQL = "INSERT INTO wb_log_login (status) " + " VALUES " + " (@status)";
                objCmd = new SqlCommand(strSQL, clsDB.objConn);
                objCmd.Parameters.Add("@status", SqlDbType.Int).Value = 1;
                objCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static int AllLogin()
        {
            strSQL = "SELECT COUNT(log_login_id) As count FROM wb_log_login";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int CurrentLogin()
        {
            strSQL = " SELECT COUNT(log_login_id) As count FROM wb_log_login " +
            " WHERE  createdate >= '" + DateTime.Now.AddMinutes(-31).AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "' " +
            " AND createdate <= '" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "' ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int LastDayLogin()
        {
            int day = DateTime.Now.Day;
            strSQL = " SELECT COUNT(log_login_id) As count FROM wb_log_login " +
            " WHERE  DAY(createdate) = " + (day - 1) + " " +
            " AND YEAR(createdate) = DATEPART(YYYY,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int WeekLogin()
        {
            strSQL = "SELECT COUNT(log_login_id) As count FROM wb_log_login " +
            " WHERE  DATEPART(WEEK,createdate) = DATEPART(WEEK,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') " +
            " AND YEAR(createdate) = DATEPART(YYYY,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int LastWeekLogin()
        {
            strSQL = "SELECT COUNT(log_login_id) As count FROM wb_log_login " +
            " WHERE  DATEPART(WEEK,createdate) = (DATEPART(WEEK,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') - 1) " +
            " AND YEAR(createdate) = DATEPART(YYYY,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int MonthLogin()
        {
            int month = DateTime.Now.Month;
            strSQL = " SELECT COUNT(log_login_id) As count FROM wb_log_login " +
            " WHERE  MONTH(createdate) = " + month + " " +
            " AND YEAR(createdate) = DATEPART(YYYY,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int LastMonthLogin()
        {
            int month = DateTime.Now.Month;
            strSQL = " SELECT COUNT(log_login_id) As count FROM wb_log_login " +
            " WHERE  MONTH(createdate) = " + (month - 1) + " " +
            " AND YEAR(createdate) = DATEPART(YYYY,'" + DateTime.Now.AddYears(-543).ToString("yyyy-MM-dd HH:mm:ss") + "') ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int CountTopicInForum(int forum_id)
        {
            strSQL = " SELECT COUNT(topic_id) As count FROM wb_topic " +
                " WHERE  forum_id = " + forum_id + " ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int CountViewInForum(int forum_id)
        {
            strSQL = " SELECT SUM(topic_view) As count FROM wb_topic " +
                " WHERE  forum_id = " + forum_id + " ";
            dt = clsData.DtTable(strSQL);
            if (dt != null && dt.Rows[0]["count"].ToString() != "")
            {
                count = Convert.ToInt32(dt.Rows[0]["count"]);
            }
            else
            {
                count = 0;
            }
            return count;
        }

        public static int CountCommentInForum(int forum_id)
        {
            strSQL = " SELECT COUNT(wb_comment.comment_id) As count FROM wb_comment" +
                " INNER JOIN wb_topic ON wb_topic.topic_id = wb_comment.topic_id " +
                " INNER JOIN wb_forum ON wb_forum.forum_id = wb_topic.forum_id " +
                " WHERE  wb_forum.forum_id = " + forum_id + " ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static DataTable LastComment(int forum_id)
        {
            strSQL = " SELECT wb_comment.user_id ,wb_comment.comment_create FROM wb_comment" +
                " INNER JOIN wb_topic ON wb_topic.topic_id = wb_comment.topic_id " +
                " INNER JOIN wb_forum ON wb_forum.forum_id = wb_topic.forum_id " +
                " WHERE  wb_forum.forum_id = " + forum_id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static DataTable LastCommentTopic(int topic_id)
        {
            strSQL = " SELECT wb_comment.user_id ,wb_comment.comment_create FROM wb_comment" +
                " INNER JOIN wb_topic ON wb_topic.topic_id = wb_comment.topic_id " +
                " INNER JOIN wb_forum ON wb_forum.forum_id = wb_topic.forum_id " +
                " WHERE  wb_topic.topic_id = " + topic_id + " ";
            dt = clsData.DtTable(strSQL);
            return dt;
        }

        public static int CountTopicByUser(int user_id)
        {
            strSQL = " SELECT COUNT(topic_id) As count FROM wb_topic " +
                " WHERE  user_id = " + user_id + " ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }

        public static int CountCommentByUser(int user_id)
        {
            strSQL = " SELECT COUNT(comment_id) As count FROM wb_comment " +
                " WHERE  user_id = " + user_id + " ";
            dt = clsData.DtTable(strSQL);
            count = Convert.ToInt32(dt.Rows[0]["count"]);
            return count;
        }
    }
}